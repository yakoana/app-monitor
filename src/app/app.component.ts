import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css', '../../node_modules/w3-css/w3.css']
})
export class AppComponent
{
	title: string = 'Dibbix';
	constructor() { }
}
