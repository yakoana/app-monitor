
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common'
import pt from '@angular/common/locales/pt';
registerLocaleData(pt);

import { AppComponent } from './app.component';
import { ServersComponent } from './servers/servers.component';
import { ServerDetailComponent } from './server-detail/server-detail.component';
import { ConfigComponent } from './config/config.component';

@NgModule({
	declarations: [
		AppComponent,
		ServersComponent,
		ServerDetailComponent,
		ConfigComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		FormsModule
	],
	providers: [
		{ provide: LOCALE_ID, useValue: 'pt' }
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
