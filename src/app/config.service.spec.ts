import { TestBed } from '@angular/core/testing';
import { ConfigService, PersistedConfig, PersistedRules } from './config.service';


describe('ConfigService', () => {
	let service: ConfigService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(ConfigService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('#getConfig should return Config', () => {
		expect(service.getConfig()).toBeInstanceOf(PersistedConfig);
	});

	it ('config must return rules', () => {
		expect(service.getConfig().rules).toBeInstanceOf(PersistedRules);
	});
});
