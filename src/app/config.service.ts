import { Injectable } from '@angular/core';
import { Config, Rules } from './types';

@Injectable({
	providedIn: 'root'
})
export class ConfigService
{
	static readonly STORAGE_KEY = 'dibbix-config';
	cfg: Config;
	constructor()
	{
		let ob: string | null = window.localStorage.getItem(ConfigService.STORAGE_KEY);
		let conf: PersistedConfig = new PersistedConfig();
		if (ob) conf.from(JSON.parse(ob));
		else window.localStorage.setItem(ConfigService.STORAGE_KEY, JSON.stringify(conf));
		this.cfg = conf;

	}
	getConfig(): Config { return this.cfg; }
	setConfig(newCfg: Config): void { this.cfg = Object.assign({}, newCfg); }
	store(): void
	{
		try
		{
			window.localStorage.removeItem(ConfigService.STORAGE_KEY);
			window.localStorage.setItem(ConfigService.STORAGE_KEY, JSON.stringify(this.cfg));
		}
		catch (e)
		{
			if
			(
				e instanceof DOMException &&
				(e.code === 22 || e.code === 1014 || e.name === 'QuotaExceededError' || e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
				window.localStorage.length !== 0
			)	console.error('Storage local do navegador excedeu a quota');
			else console.error('Erro inesperado ao gravar a configuração' + JSON.stringify(e));
		}
	}
}
export class PersistedRules implements Rules
{
	lastknowof: number = 10;
	ratio: number = 2;
	failures: number = 6;
	green: number = 8;
	red: number = 12;
	devider: number = 1.5;
	limit: number = 610329e-10;
}
export class PersistedConfig implements Config
{
	reload: number = 30000;
	repeat: number = 3;
	rules: Rules = new PersistedRules();
	from(ob: any): void
	{
		if (ob)
		{
			if (ob.reload) this.reload = ob.reload;
			if (ob.repeat) this.repeat = ob.repeat;
			if (ob.rules.lastknowof) this.rules.lastknowof = ob.rules.lastknowof;
			if (ob.rules.ratio) this.rules.ratio = ob.rules.ratio;
			if (ob.rules.failures) this.rules.failures = ob.rules.failures;
			if (ob.rules.green) this.rules.green = ob.rules.green;
			if (ob.rules.red) this.rules.red = ob.rules.red;
			if (ob.rules.devider) this.rules.devider = ob.rules.devider;
			if (ob.rules.limit) this.rules.limit = ob.rules.limit;
		}
	}
}
