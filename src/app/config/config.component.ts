import { Component, OnInit } from '@angular/core';
import { ConfigService, PersistedConfig } from '../config.service';
import { Config } from '../types';

@Component({
	selector: 'app-config',
	templateUrl: './config.component.html',
	styleUrls: ['./config.component.css', '../../../node_modules/w3-css/w3.css']
})

export class ConfigComponent implements OnInit
{
	config: Config;
	backup: Config;
	constructor(private cfg: ConfigService) { }

	private loadConfig(): Config
	{
		let ret: Config = new PersistedConfig();
		ret.reload = this.cfg.getConfig().reload;
		ret.repeat = this.cfg.getConfig().repeat;
		ret.rules.devider = this.cfg.getConfig().rules.devider;
		ret.rules.failures = this.cfg.getConfig().rules.failures;
		ret.rules.green = this.cfg.getConfig().rules.green;
		ret.rules.lastknowof = this.cfg.getConfig().rules.lastknowof;
		ret.rules.limit = this.cfg.getConfig().rules.limit;
		ret.rules.ratio = this.cfg.getConfig().rules.ratio;
		ret.rules.red = this.cfg.getConfig().rules.red;
		return ret;
	}
	ngOnInit(): void
	{
		this.backup = this.loadConfig();
		this.config = this.cfg.getConfig();
	}
	onSubmit(): void
	{
		this.cfg.setConfig(this.config);
		this.cfg.store();
		this.ngOnInit();
		document.getElementById('config').style.display='none';
	}
	onReset(): void
	{
		this.config = new PersistedConfig();
	}
	onCancel(): void
	{
		this.cfg.setConfig(this.backup);
		this.ngOnInit();
		document.getElementById('config').style.display='none';
	}
}
