import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Server, Event } from './types';
import { environment } from '../environments/environment';
import { ConfigService } from './config.service';


@Injectable({
	providedIn: 'root'
})
export class RestApiService
{
	constructor(private http: HttpClient, private cfg: ConfigService) { }
	private handle(err: HttpErrorResponse): Observable<never>
	{
		if (err.error instanceof ErrorEvent) console.error('Falha no acesso ao serviço: ${err.error.message}');
		else console.error('Serviço retornou status ${err.status} com a seguinte mensagem: ${err.error}');
		return throwError('Falha no acesso ao serviço.');
	}
	private getBaseURI(type: Services): string
	{
		let uri: string = environment.targetURI;
		switch (type)
		{
		case Services.Agents: uri += '/agents';
			break;
		case Services.Servers: uri += '/servers';
			break;
		case Services.Rogues: uri += '/rogueservers';
			break;
		}
		return uri;
	}
	listServices(type: Services):  Observable<Array<string>>
	{
		return this.http
			.get<Array<string>>(this.getBaseURI(type))
			.pipe(retry(this.cfg.getConfig().repeat), catchError(this.handle));
	}
	getService(type: Services, name: string): Observable<Server>
	{
		return this.http
			.get<Server>(this.getBaseURI(type) + '/' + encodeURIComponent(name))
			.pipe(retry(this.cfg.getConfig().repeat), catchError(this.handle));
	}
	getEvents(type: Services, name: string): Observable<Array<Event>>
	{
		return this.http
			.get<Array<Event>>(this.getBaseURI(type) + '/' + encodeURIComponent(name) + '/events')
			.pipe(retry(this.cfg.getConfig().repeat), catchError(this.handle));
	}
	demote(name: string): Observable<any>
	{
		return this.http
			.get(this.getBaseURI(Services.Servers) + '/' + encodeURIComponent(name) + '/demote')
			.pipe(retry(this.cfg.getConfig().repeat), catchError(this.handle));
	}
	promote(name: string): Observable<any>
	{
		return this.http
			.get(this.getBaseURI(Services.Rogues) + '/' + encodeURIComponent(name) + '/promote')
			.pipe(retry(this.cfg.getConfig().repeat), catchError(this.handle));
	}
}
export enum Services
{
	Agents,
	Servers,
	Rogues
}
