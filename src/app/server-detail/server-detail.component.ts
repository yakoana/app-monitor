import { Component, OnInit, Input } from '@angular/core';
import { ServerItem } from '../types';

@Component({
	selector: 'app-server-detail',
	templateUrl: './server-detail.component.html',
	styleUrls: ['./server-detail.component.css']
})
export class ServerDetailComponent implements OnInit
{
	@Input() item: ServerItem;
	constructor() { }
	ngOnInit(): void { }
	onClose() { this.item = null; }
}
