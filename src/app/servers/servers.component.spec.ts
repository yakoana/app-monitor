import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ServersComponent } from './servers.component';
import { RestApiService } from '../rest-api.service';
import { ConfigService } from '../config.service';

describe('ServersComponent', () => {
	let component: ServersComponent;
	let fixture: ComponentFixture<ServersComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ ServersComponent ],
			providers: [ RestApiService, ConfigService ]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ServersComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
