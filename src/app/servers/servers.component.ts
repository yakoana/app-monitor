import { Component, OnInit, OnDestroy } from '@angular/core';
import { RestApiService, Services } from '../rest-api.service';
import { Server, Event, ServerItem, Status } from '../types';
import { timer, Observable, Subscription } from 'rxjs';
import { ConfigService } from '../config.service';


@Component({
	selector: 'app-servers',
	templateUrl: './servers.component.html',
	styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit, OnDestroy
{
	agentServers: Array<ServerItem> = [];
	knownServers: Array<ServerItem> = [];
	rogueServers: Array<ServerItem> = [];
	item: ServerItem = null;
	interval: Observable<number> = null;
	subscription: Subscription = null;
	monitor: Map<string, Take> = new Map();
	constructor(private restApi: RestApiService, private cfg: ConfigService) { }

	private listServices(type: Services): Array<ServerItem>
	{
		let target: Array<ServerItem> = [];
		let i: number = 0;
		this.restApi.listServices(type).subscribe
		(
			(res: Array<string>) =>
			{
				res.forEach((value: string) =>
				{
					this.restApi.getService(type, value).subscribe
					(
						(svr: Server) =>
						{
							let item: ServerItem = new ServerItem();
							item.name = svr.name;
							item.isOdd = (i++ % 2) == 1 ? true : false;
							item.status = this.calculateStatus(svr);
							item.server = svr;
							target.push(item);
						},
						(error: any) => { console.error('Falha na obtenção do serviço ' + value + ': ' + JSON.stringify(error)); }
					);
				});
			},
			(error: any) => { console.error('Falha na obtenção dos serviços em execução: ' + JSON.stringify(error)); }
		);
		return target;
	}
	private fillDetails(target: string, from: Array<ServerItem>, type: Services): void
	{
		this.item= null;
		let stored: ServerItem =  from.find(x => x.name == target);
		if (!stored) throw 'Nome do serviço inválido';
		this.restApi.getService(type, stored.name).subscribe
		(
			(svr: Server) =>
			{
				this.restApi.getEvents(type, svr.name).subscribe
				(
					(res: Array<Event>) =>
					{
						this.item = stored;
						this.item.server.events = '';
						res.forEach((evt: Event) =>
						{
							this.item.server.events += '<p>' + ServerEvent.stringfy(evt) + '</p>';
						});
					},
					(error: any) => { console.error('Falha na obtenção dos eventos do service ' + target + ': ' + JSON.stringify(error)); }
				);
			},
			(error: any) => { console.error('Falha na obtenção do serviço ' + target + ': ' + JSON.stringify(error)); }
		);
	}
	private calculateStatus(res: Server): Status
	{
		let last: Take | undefined = this.monitor.get(res.name);
		let current: Take = new Take();
		current.name = res.name;
		current.startTime = res.startTime;
		current.lastknowof = res.lastknowof;
		current.success = res.success;
		current.failures = res.failures;
		let points: number = 0;
		if (last)
		{
			if (last.startTime == current.startTime)
			{
				if (last.lastknowof >= current.lastknowof) points += this.cfg.getConfig().rules.lastknowof;
				let lastRatio: number;
				if (last.success == 0 && last.failures != 0) lastRatio = 1;
				if (last.success == 0 && last.failures == 0) lastRatio = 0;
				else lastRatio = last.failures / last.success;
				let curRatio: number;
				if (current.success == 0 && current.failures != 0) curRatio = 1;
				if (current.success == 0 && current.failures == 0) curRatio = 0;
				else curRatio = current.failures / current.success;
				if (lastRatio < curRatio || curRatio == 1) points += this.cfg.getConfig().rules.ratio;
				if (last.failures < current.failures) points += this.cfg.getConfig().rules.failures;
				points += last.points;
				points /= this.cfg.getConfig().rules.devider;
			}
			current.points = points;
			current.rounds = last.rounds + 1;
			if (current.points < this.cfg.getConfig().rules.limit) current.points = 0;
		}
		else last = Object.assign({}, current);

		this.monitor.set(res.name, current);
		let ret = new Status();
		if (points <= this.cfg.getConfig().rules.green)
		{
			ret.src = 'assets/green.png';
			ret.alt = 'Processamento sem problemas';
			ret.title = 'Processamento sem problemas';
			ret.class = 'green';
		}
		else if (points > this.cfg.getConfig().rules.red)
		{
			ret.src = 'assets/red.png';
			ret.alt = 'Alarme! Serviço com graves problemas';
			ret.title = 'Alarme! Serviço com graves problemas';
			ret.class = 'red';
		}
		else
		{
			ret.src = 'assets/yellow.png';
			ret.alt = 'Atenção: serviço com problemas';
			ret.title = 'Atenção: serviço com problemas';
			ret.class = 'yellow';
		}
		return ret;
	}
	private onRefresh(): void
	{
		this.item = null;
		this.agentServers = this.listServices(Services.Agents);
		this.knownServers = this.listServices(Services.Servers);
		this.rogueServers = this.listServices(Services.Rogues);
	}

	onSelect(selected: string): void
	{
		this.fillDetails(selected, this.knownServers, Services.Servers);
	}
	onSelectAgent(selected: string): void
	{
		this.fillDetails(selected, this.agentServers, Services.Agents);
	}
	onDemote(selected: string): void
	{
		this.restApi.demote(selected).subscribe
		(
			(res: any) =>
			{
				this.monitor.delete(selected);
				this.onRefresh();
			},
			(error: any) => { console.error('Falha na promoção do serviço ' + selected + ': ' + JSON.stringify(error)); }
		);
	}
	onPromote(selected: string): void
	{
		this.restApi.promote(selected).subscribe
		(
			(res: any) => { this.onRefresh(); },
			(error: any) => { console.error('Falha na promoção do serviço ' + selected + ': ' + JSON.stringify(error)); }
		);
	}
	ngOnInit(): void
	{
		this.interval = timer(0, this.cfg.getConfig().reload);
		this.subscription = this.interval.subscribe((x: number) => { this.onRefresh(); });
	}
	ngOnDestroy(): void
	{
		if (this.subscription) this.subscription.unsubscribe();
	}
}
class ServerEvent implements Event
{
	type: string = '';
	root: string = '';
	timestamp: number = 0;
	servername: string = '';
	jbossname: string = '';
	heartbeat: {
		jmx: {
			address: string;
			port: number;
		};
		success: number;
		failures: number;
		workers: number;
	};
	contents: string = '';
	static stringfy(evt: Event): string
	{
		let line: string = '';
		line += 'Tipo: ' + evt.type + ', ';
		line += 'OID: ' + evt.root + ', ';
		line += 'Instante: ' + (new Date()).setTime(evt.timestamp).toLocaleString() + ', ';
		line += 'Servidor: ' + evt.servername + ', ';
		line += 'JBoss: ' + evt.jbossname + ', ';
		line += 'Mensagem: ' + evt.contents;
		if (evt.heartbeat)
		{
			line += ', Sinal: [JMX: ' + evt.heartbeat.jmx.address + '/' + evt.heartbeat.jmx.port + ', ';
			line += 'sucessos: ' + evt.heartbeat.success + ', ';
			line += 'falhas: ' + evt.heartbeat.failures + ', ';
			line += 'threads: ' + evt.heartbeat.workers + ']';
		}
		return line;
	}
}
class Take
{
	name: string = '';
	rounds: number = 0;
	startTime: number = 0;
	lastknowof: number = 0;
	success: number = 0;
	failures: number = 0;
	points: number = 0;
}
