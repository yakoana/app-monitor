export interface Server
{
	name: string;
	config: string;
	env: string[];
	jmx: {
		address: string;
		port: number;
	};
	lastknowof: number;
	startTime: number;
	success: number;
	failures: number;
	workers: number;
	heap: {
		init: number;
		used: number;
		committed: number;
		max: number;
	};
	nonheap: {
		init: number;
		used: number;
		committed: number;
		max: number;
	};
	heartbeats: number;
	events: string;
}
export interface Event
{
	type: string;
	root: string;
	timestamp: number;
	servername: string;
	jbossname: string;
	heartbeat: {
		jmx: {
			address: string;
			port: number;
		};
		success: number;
		failures: number;
		workers: number;
	}
	contents: string;
}
export interface Rules
{
	lastknowof: number;	// last(lastknowof) < current(lastknowof) (default 10)
	ratio: number;		// last(success/failures) > current(success/failures) (default 2)
	failures: number;	// last(failures) < current(failures) (default 6)
	green: number;		// green status (default 8)
	red: number;		// red status (default 12)
	devider: number;	// points devider (default 1.5)
	limit: number;		// when points tend to zero (default 610329e-10)
}
export interface Config
{
	reload: number;		// Reload data from things-mgmt interval (in milisseconds) (default 30000)
	repeat: number;		// Retry get data from things-mgmt (default 3)
	rules: Rules;
}
export class Status
{
	src: string = '';
	alt: string = '';
	title: string = '';
	class: string = '';
}
export class ServerItem
{
	name: string = '';
	isOdd: boolean = false;
	status: Status = null;
	server: Server = null;
}
